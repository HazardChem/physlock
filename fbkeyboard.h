#ifndef FBKEYBOARD_H
#define FBKEYBOARD_H

#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <linux/fb.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <linux/vt.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <stdint.h>

/* keyboard.c */
void fill_rect(int x, int y, int w, int h, int color);

void draw_char(int x, int y, char c);

void draw_text(int x, int y, char *text);

void draw_key(int x, int y, int w, int h, int color);

void draw_textbutton(int x, int y, int w, int h, int color, char *text);

void draw_button(int x, int y, int w, int h, int color, char chr);

void draw_keyboard(int row, int pressed);

void show_fbkeyboard(int fbfd);

int check_input_events(int fdinput, int *x, int *y);

void identify_touched_key(int x, int y, int *row, int *pressed);

void send_key(__u16 code);

void send_uinput_event(int row, int pressed);

int reset_window_size(int fd);

void set_window_size(int fd);

void term(int signum);

int keyboard_main();

#endif /* KEYBOARD_H */
